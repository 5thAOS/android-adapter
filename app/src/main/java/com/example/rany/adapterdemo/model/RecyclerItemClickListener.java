package com.example.rany.adapterdemo.model;

public interface RecyclerItemClickListener {

    void onItemClick(int position);

}
