package com.example.rany.adapterdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.rany.adapterdemo.model.Article;

import java.util.ArrayList;
import java.util.List;

public class CustomListViewActivity extends AppCompatActivity {

    ListView lvArticle ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_list_view);

        lvArticle = findViewById(R.id.lvArticle);

        final List<Article> articles = new ArrayList<>();
        for(int i = 1; i <= 20; i++){
            articles.add(new Article(i, "Article "+ i));
        }

        CustomListViewAdapter adapter = new CustomListViewAdapter(articles);
        lvArticle.setAdapter(adapter);
        lvArticle.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String a_id;
                String a_title;
                Article article = articles.get(position);
                a_id = String.valueOf(article.getId());
                a_title = article.getTitle();
                Toast.makeText(CustomListViewActivity.this,a_id+" "+a_title , Toast.LENGTH_SHORT).show();
            }
        });

    }
}
