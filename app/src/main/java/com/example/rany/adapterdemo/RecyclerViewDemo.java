package com.example.rany.adapterdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.rany.adapterdemo.model.Article;
import com.example.rany.adapterdemo.model.RecyclerItemClickListener;

import java.util.ArrayList;

public class RecyclerViewDemo extends AppCompatActivity implements RecyclerItemClickListener {

    RecyclerView rcArticle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view_demo);

        rcArticle = findViewById(R.id.rcArticle);

        ArrayList<Article> articles = new ArrayList<>();
        for(int i = 1; i <= 30; i++){
            articles.add(new Article(i, "Title "+i));
        }
        ArticleAdapter articleAdapter = new ArticleAdapter();
        articleAdapter.onRecyclerClickListener(this);
        rcArticle.setLayoutManager(new LinearLayoutManager(this));
        rcArticle.setAdapter(articleAdapter);
        articleAdapter.addArticles(articles);
    }

    @Override
    public void onItemClick(int position) {
        Toast.makeText(this, position+" ", Toast.LENGTH_SHORT).show();
    }
}
