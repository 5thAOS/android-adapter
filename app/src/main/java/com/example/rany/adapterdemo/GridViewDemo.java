package com.example.rany.adapterdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.GridView;

public class GridViewDemo extends AppCompatActivity {

    GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view_demo);

        gridView = findViewById(R.id.gvView);

        String[] programming = {
                "HTML", "CSS", "JavaScript", "Bootstrap", "jQuery", "AJAX","JSON",
                "Java", "Swift","Python", "VB", "Ruby on Rail", "VBA", "C++", "C#"
        };

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, programming);

        gridView.setAdapter(arrayAdapter);



    }
}
