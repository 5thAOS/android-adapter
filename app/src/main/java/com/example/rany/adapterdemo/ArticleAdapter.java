package com.example.rany.adapterdemo;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rany.adapterdemo.model.Article;
import com.example.rany.adapterdemo.model.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.ArticleViewHolder> {

    List<Article> articles;
    RecyclerItemClickListener listener;

    public void onRecyclerClickListener(RecyclerItemClickListener listener){
        this.listener = listener;
    }

    public ArticleAdapter(){
        articles = new ArrayList<>();
    }

    public void addArticles(List<Article> article){
        articles.addAll(article);
        notifyDataSetChanged();
    }
//    public ArticleAdapter(List<Article> articles) {
//        this.articles = articles;
//    }

    @NonNull
    @Override
    public ArticleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item, parent, false);

        return new ArticleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleViewHolder holder, int position) {
        Article article = articles.get(position);
        holder.tId.setText(String.valueOf(article.getId()));
        holder.tTitle.setText(article.getTitle());
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    class ArticleViewHolder extends RecyclerView.ViewHolder{

        TextView tId, tTitle;

        public ArticleViewHolder(final View itemView) {
            super(itemView);
            tId = itemView.findViewById(R.id.tvRID);
            tTitle = itemView.findViewById(R.id.tvRTitle);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(getAdapterPosition());
                }
            });

        }
    }

}
