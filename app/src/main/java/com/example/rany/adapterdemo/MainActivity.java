package com.example.rany.adapterdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ListView lvModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final String[] items = {"Samsung", "Huwei", "Singtech", "Sony", "Motorola","Blackberry", "ASUS",
                "Nokia", "iPhone", "iPad", "iWatch", "OPPO", "one plus", "Xiao Mi", "Vivo", "Lollipop"};


        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_list_item_1, items);

        lvModel = findViewById(R.id.lvModel);
        lvModel.setAdapter(arrayAdapter);
        lvModel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivity.this, items[position], Toast.LENGTH_SHORT).show();
            }
        });

    }
}
