package com.example.rany.adapterdemo;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.rany.adapterdemo.model.Article;

import java.util.ArrayList;
import java.util.List;

public class CustomListViewAdapter extends BaseAdapter {

    List<Article> articles = new ArrayList<>();

    public CustomListViewAdapter(List<Article> articles) {
        this.articles = articles;
    }

    @Override
    public int getCount() {
        return articles.size();
    }

    @Override
    public Object getItem(int position) {
        return articles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_list_view, parent, false);

        TextView tvID = view.findViewById(R.id.tvID);
        TextView tvTitle = view.findViewById(R.id.tvTitle);

        Article article = articles.get(position);
        tvID.setText(String.valueOf(article.getId()));
        tvTitle.setText(article.getTitle());

        return view;
    }
}
